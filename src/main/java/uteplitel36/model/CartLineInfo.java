package uteplitel36.model;

public class CartLineInfo {

    private ProductInfo productInfo;
    private int quantity;

    public CartLineInfo() {
        this.quantity = 0;
    }

    public ProductInfo getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

//    public boolean isFloatA(){
//        return (this.productInfo.getPrice() * this.quantity) % 1 != 0;
//    }
//
//    public boolean isFloat(){
//        return this.productInfo.getPrice() % 1 != 0;
//    }

    public double getAmount() {
        return this.productInfo.getPrice() * this.quantity;
    }

//    public String getAmountF() {
//        String[] s = String.valueOf(this.productInfo.getPrice() * this.quantity).split(",");
//        return s[0]+"."+s[1]+"0р.";
//    }
}

package uteplitel36.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;
import uteplitel36.dao.ProductDAO;
import uteplitel36.domain.Category;
import uteplitel36.domain.Item;
import uteplitel36.model.CartInfo;
import uteplitel36.repos.CatalogRepo;
import uteplitel36.repos.CategoryRepo;
import uteplitel36.repos.SubCategoryRepo;
import uteplitel36.util.Utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Path;
import java.util.Locale;

@Controller
public class CatalogController {
    private final CatalogRepo catalogRepo;
    private final CategoryRepo categoryRepo;
    private final SubCategoryRepo subCategoryRepo;
    private final ProductDAO productDAO;
    private Path sitemapDirectory;
    final ResourceLoader resourceLoader;

    @Autowired
    public CatalogController(CatalogRepo catalogRepo, CategoryRepo categoryRepo, SubCategoryRepo subCategoryRepo, ProductDAO productDAO, ResourceLoader resourceLoader) {
        this.catalogRepo = catalogRepo;
        this.categoryRepo = categoryRepo;
        this.subCategoryRepo = subCategoryRepo;
        this.productDAO = productDAO;
        this.resourceLoader = resourceLoader;
    }

    @RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource getFile(HttpServletResponse response) throws IOException, SAXException, ParserConfigurationException {
        response.setContentType("application/xml");
        ClassPathResource cpr = new ClassPathResource("templates/sitemap.xml");

        BufferedReader reader = new BufferedReader(new InputStreamReader( cpr .getInputStream()));
        String str = FileCopyUtils.copyToString( reader );

        File output = new File("sitemap.xml");
        FileWriter writer = new FileWriter(output);

        writer.write(str);
        writer.flush();
        writer.close();
        return new FileSystemResource(output); //Or path to your file
    }

    @GetMapping("yandex_9f81748738619c9b.html")
    public String ya() {

        return "yandex_9f81748738619c9b";
    }

    @GetMapping("/")
    public String home(
            HttpServletRequest request,
            Model model
    ) {
        CartInfo myCart = Utils.getCartInSession(request);

        model.addAttribute("cartSize", myCart.getCartLines().size());
        return "home";
    }

    @GetMapping("/about")
    public String about(
            HttpServletRequest request,
            Model model
    ) {
        CartInfo myCart = Utils.getCartInSession(request);

        model.addAttribute("cartSize", myCart.getCartLines().size());
        return "about";
    }

    @GetMapping("/payment")
    public String payment(
            HttpServletRequest request,
            Model model
    ) {
        CartInfo myCart = Utils.getCartInSession(request);

        model.addAttribute("cartSize", myCart.getCartLines().size());
        return "payment";
    }

    @GetMapping("/shipping")
    public String shipping(
            HttpServletRequest request,
            Model model
    ) {
        CartInfo myCart = Utils.getCartInSession(request);

        model.addAttribute("cartSize", myCart.getCartLines().size());
        return "shipping";
    }

    @GetMapping("/contacts")
    public String contacts(
            HttpServletRequest request,
            Model model
    ) {
        CartInfo myCart = Utils.getCartInSession(request);

        model.addAttribute("cartSize", myCart.getCartLines().size());
        return "contacts";
    }

    @GetMapping("/item/{code}")
    public String item(
            HttpServletRequest request,
            @PathVariable String code,
            Model model
    ) {
        CartInfo myCart = Utils.getCartInSession(request);
        Item product = productDAO.findProduct(code);

        model.addAttribute("cartSize", myCart.getCartLines().size());
        model.addAttribute("product", product);
        model.addAttribute("title", product.getTitle());
        model.addAttribute("description_h", product.getH_description());
        model.addAttribute("keyword", product.getKeyWord());

        return "item";
    }

    @GetMapping("/brands/{brand}")
    public String brands(
            HttpServletRequest request,
            @PathVariable String brand,
            Model model,
            @PageableDefault(size = 8, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {
        CartInfo myCart = Utils.getCartInSession(request);
        String title = "";
        String description_h = "";
        String keyword = "";

        switch (brand) {
            case "penoplex":
                title = "Купить теплоизоляционные материалы по самым низким ценам в городе Воронеж.";
                description_h = "Широкий выбор теплоизоляционных материалов по самым низким ценам в г.Воронеж. Всегда в наличии, быстрая доставка, возможно скидки. Звоните +7 (980) 545-93-15";
                keyword = "Утеплитель воронеж купить, пенополистирол воронеж, пеноплекс воронеж, стекловата воронеж купить, теплоизоляция воронеж купить";
                break;
            case "isovol":
                title = "Купить минеральную вату 'Isovol' в Воронеже по низкой цене.";
                description_h = "Теплоизоляция IZOVOL (ИЗОВОЛ)предназначена для эффективной теплоизоляции,пожароизоляции,звукоизоляции различных ненагружаемых конструкций.Всегда в наличии, быстрая доставка, возможно скидки. Звоните +7 (980) 545-93-15";
                keyword = "изовол воронеж купить, стекловата воронеж купить, изовол утеплитель купить воронеж, isovol купить в воронеже";
                break;
        }

        model.addAttribute("cartSize", myCart.getCartLines().size());
        Page<Item> page = catalogRepo.findByBrand(pageable, brand);
        model.addAttribute("page", page);
        model.addAttribute("categoryName", page.getContent().get(0).getCategory());
        model.addAttribute("title", title);
        model.addAttribute("description_h", description_h);
        model.addAttribute("keyword", keyword);
        model.addAttribute("brand", brand.toUpperCase(Locale.ROOT));
        model.addAttribute("url", "/brands/" + brand);

        return "brand";
    }

    @GetMapping("/categories/{categoryName}")
    public String category(
            HttpServletRequest request,
            @PathVariable String categoryName,
            Model model,
            @PageableDefault(size = 8, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) {

        CartInfo myCart = Utils.getCartInSession(request);
        model.addAttribute("cartSize", myCart.getCartLines().size());
        Category category = categoryRepo.findByName(categoryName);
        Page<Item> page = catalogRepo.findByCategory(pageable, category.getName());
        model.addAttribute("page", page);
        model.addAttribute("category", category);
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("url", "/categories/" + categoryName);
        model.addAttribute("title", category.getTitle());
        model.addAttribute("description_h", category.getDescription_h());
        model.addAttribute("keyword", category.getKeyword());

        return "category";
    }

//    @GetMapping("/categories/{category}/{subCategoryName}")
//    public String subCategory(
//            HttpServletRequest request,
//            @PathVariable String category,
//            @PathVariable String subCategoryName,
//            Model model,
//            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable
//    ){
//        CartInfo myCart = Utils.getCartInSession(request);
//
//        model.addAttribute("cartSize", myCart.getCartLines().size());
//        SubCategory subCategory = subCategoryRepo.findByName(subCategoryName);
//        Page<Item> page = catalogRepo.findBySubCategory(pageable, category, subCategory.getName());
//        model.addAttribute("page", page);
//        model.addAttribute("subCategory", subCategory);
//        model.addAttribute("categoryName", category);
//        model.addAttribute("url", "/categories/"+ category +"/" + subCategoryName);
//
//        return "subcategory";
//    }
}

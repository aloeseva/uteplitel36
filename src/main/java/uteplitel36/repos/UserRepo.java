package uteplitel36.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import uteplitel36.domain.User;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
//    User findByUsername(String username);
    User findByIdExt(String idExt);
    User findByEmail(String email);

//    User findByActivationCode(String code);
}

package uteplitel36.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uteplitel36.domain.Order;
import uteplitel36.model.CartInfo;
import uteplitel36.model.OrderDetailInfo;
import uteplitel36.model.OrderInfo;

import java.util.List;
import java.util.Set;

public interface OrderDAO {

    public void saveOrder(CartInfo cartInfo, boolean isUserAuth);

    public Page<OrderInfo> listOrderInfo(Pageable pageable);

    public Page<OrderInfo> listOrderInfoForUser(Pageable pageable, Long id);

    public OrderInfo getOrderInfo(String orderId);

    public List<OrderDetailInfo> listOrderDetailInfos(String orderId);

}

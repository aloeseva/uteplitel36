package uteplitel36.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import uteplitel36.model.CartInfo;
import uteplitel36.model.CustomerInfo;

@Service
public class CatalogService {
    @Autowired
    private MailSender mailSender;

    public void sendMessage(CartInfo cartInfo) {
        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        if (!StringUtils.isEmpty(customerInfo.getEmail())) {
            String message = String.format(
                    "Спасибо за заказ, %s! \n" +
                            "Информация заказа \n" +
                            "Информация заказчика: \n" +
                            "Имя: %s \n" +
                            "Фамилия: %s \n" +
                            "Email: %s \n" +
                            "Телефон: %s \n" +
                            "Адрес: %s \n" +
                            "Способ получения: %s \n" +
                            "Итог по заказу: \n" +
                            "Сумма: %s \n",
                    customerInfo.getFirstname(),
                    customerInfo.getFirstname(),
                    customerInfo.getSecondname(),
                    customerInfo.getEmail(),
                    customerInfo.getPhone(),
                    customerInfo.getAddress(),
                    customerInfo.getShipping(),
                    cartInfo.getAmountTotal()
            );

            mailSender.send(customerInfo.getEmail(), "Информация заказа uteplitel36", message);
        }
    }

    public void sendMessageToBenis(CartInfo cartInfo) {
        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        if (!StringUtils.isEmpty(customerInfo.getEmail())) {
            String message = String.format(
                    "Спасибо за заказ, %s! \n" +
                            "Информация заказа \n" +
                            "Информация заказчика: \n" +
                            "Имя: %s \n" +
                    "Фамилия: %s \n" +
                    "Email: %s \n" +
                    "Телефон: %s \n" +
                    "Адрес: %s \n" +
                    "Способ получения: %s \n" +
                    "Итог по заказу: \n" +
                    "Сумма: %s \n",
                    customerInfo.getFirstname(),
                    customerInfo.getFirstname(),
                    customerInfo.getSecondname(),
                    customerInfo.getEmail(),
                    customerInfo.getPhone(),
                    customerInfo.getAddress(),
                    customerInfo.getShipping(),
                    cartInfo.getAmountTotal()
//                    "adad"
//                    user.getActivationCode()
            );

            mailSender.send("uteplitel36@yandex.ru", "Информация заказа uteplitel36", message);
        }
    }
}

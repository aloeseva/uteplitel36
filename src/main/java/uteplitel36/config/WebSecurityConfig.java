package uteplitel36.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import uteplitel36.service.UserSevice;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserSevice userSevice;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public WebSecurityConfig(UserSevice userSevice, PasswordEncoder passwordEncoder) {
        this.userSevice = userSevice;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/", "/registration", "/about", "/shipping", "/shoppingCartFinalize", "/shoppingCartCustomer", "/shoppingCartConfirmation", "/shoppingCartRemoveProduct", "/shoppingCart", "/buyProduct", "/category/**", "/contacts", "/brands/**", "/categories/**", "/item/**", "/payment", "/static/fonts/**", "/static/css/**", "/static/js/**", "/static/img/**", "/static/images/**", "/yandex_9f81748738619c9b.html", "/sitemap.xml").permitAll()
                    .anyRequest().authenticated()
                .and()
                    .exceptionHandling()
                    .accessDeniedPage("/403");

        http
                .authorizeRequests()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/")
                    .failureUrl("/login.ftl?error=true")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .permitAll()
                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/")
                    .permitAll();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userSevice)
                .passwordEncoder(passwordEncoder);
    }
}

-- -----------------------------------------------------
-- Table `hibernate_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS hibernate_sequence
(
    `next_val` BIGINT NOT NULL
)
    AUTO_INCREMENT = 1;

insert into hibernate_sequence (next_val)
values (47);

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS usr
(
    `id`         BIGINT       NOT NULL,
    `id_ext`     varchar(50)  NOT NULL unique,
    `firstname`  VARCHAR(255),
    `secondname` VARCHAR(255),
    `password`   VARCHAR(255) NOT NULL,
    `telephone`  VARCHAR(255),
    `email`      VARCHAR(255),
    `active`     BOOLEAN      NOT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS user_role
(
    `user_id` BIGINT NOT NULL,
    `roles`   VARCHAR(255),
    INDEX user_role_user_fk (`user_id` ASC),
    CONSTRAINT user_role_user_fk
        FOREIGN KEY (`user_id`)
            REFERENCES usr (`id`)
);

-- -----------------------------------------------------
-- Table `item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS item
(
    `id`           BIGINT NOT NULL,
    `code`         VARCHAR(20),
    `name`         VARCHAR(255),
    `brand`        VARCHAR(255),
    `category`     VARCHAR(255),
    `sub_category` VARCHAR(255),
    `price`        FLOAT,
    `image`        VARCHAR(255),
    `description`  VARCHAR(255),
    `model`        VARCHAR(255),
    `measure`      VARCHAR(255),
    `square`       FLOAT,
    `capacity`     FLOAT,
    `application`  VARCHAR(255),
    `key_word`      VARCHAR(255),
    `in_stock`      BOOLEAN,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS category
(
    `id`            BIGINT NOT NULL,
    `name`          VARCHAR(255),
    `description`   VARCHAR(255),
    `description_h` VARCHAR(255),
    `keyword`       VARCHAR(255),
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `sub_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS sub_category
(
    `id`          BIGINT NOT NULL,
    `name`        VARCHAR(255),
    `description` VARCHAR(255),
    `keyword`     VARCHAR(255),
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS orders
(
    ID                  varchar(50)      not null,
    Amount              double precision not null,
    Customer_Address    varchar(255)     not null,
    Customer_Email      varchar(128)     not null,
    Customer_Firstname  varchar(255)     not null,
    Customer_Secondname varchar(255)     not null,
    Customer_Phone      varchar(128)     not null,
    Customer_Shipping   varchar(128)     not null,
    Order_Date          datetime         not null,
    Order_Num           integer          not null,
    user_id             BIGINT           NOT NULL,
    primary key (ID),
    CONSTRAINT test_fk2
        FOREIGN KEY (user_id)
            REFERENCES usr (`id`)
);

-- -----------------------------------------------------
-- Table `Order_Details`
-- -----------------------------------------------------
create table order_details
(
    ID         varchar(50)      not null,
    Amount     double precision not null,
    Price      double precision not null,
    Quanity    integer          not null,
    ORDER_ID   varchar(50)      not null,
    PRODUCT_ID BIGINT           not null,
    primary key (ID)
);

alter table orders
    add constraint UK_sxhpvsj665kmi4f7jdu9d2791 unique (Order_Num);

alter table order_details
    add constraint ORDER_DETAIL_ORD_FK
        foreign key (ORDER_ID)
            references orders (ID);

alter table order_details
    add constraint ORDER_DETAIL_PROD_FK
        foreign key (PRODUCT_ID)
            references item (id);

insert into usr (id, id_ext, firstname, secondname, password, telephone, email, active)
values ('1', '228', 'Всеволод', 'Шумбасов', '$2a$08$pKe3uV5kSb358BIotDLRhOOVSqNKvNVbiqpS/CSYyJjVc0MvF8xyG',
        '89204680443',
        '45678913579@mail.ru', true);

insert into usr (id, id_ext, firstname, secondname, password, telephone, email, active)
values ('0', '1488', 'Денис', 'Бондарев', '$2a$08$pKe3uV5kSb358BIotDLRhOOVSqNKvNVbiqpS/CSYyJjVc0MvF8xyG', '89204680443',
        '4567891357@mail.ru', true);

insert into user_role (user_id, roles)
values ('1', 'USER'),
       ('1', 'ROLE_MANAGER'),
       ('1', 'EMPLOYEE'),
       ('1', 'ADMIN');

insert into user_role (user_id, roles)
values ('0', 'USER');

insert into category(id, name, description, description_h, keyword)
values (2, 'uteplitel', 'Утеплитель', 'Купить утеплитель пеноплекс и не только в Воронеже недорого на сайте uteplitel36 или по адресу: ул. ккккккккккккк, или заказать с доставкой по тел: +7(000)000-00-00', 'утеплитель пеноплекс, утеплитель для дома, утеплитель для стен, купить утеплитель недорого, цена пеноплекс воронеж, где купить пеноплекс в воронеже, пенополистирол пеноплекс, пеноплекс размер, пеноплекс стена, пеноплекс фундамент, пеноплекс комфорт, сколь');

insert into category(id, name, description, keyword)
values (3, 'plenki', 'Пленки', 'Защитную пленку купить в Воронеже на сайте uteplitel36 или по адресу: ул. ккккккккккккк или заказать с доставкой по тел: +7(000)000-00-00');

insert into category(id, name, description, keyword)
values (4, 'pena', 'Пены', 'Пена монтажная профессиональная купить в Воронеже на сайте uteplitel36 или по адресу: ул. ккккккккккккк или заказать с доставкой по тел: +7(000)000-00-00');

insert into category(id, name, description, keyword)
values (5, 'krepez', 'Крепеж', 'Крепеж для различного утеплителя купить в Воронеже на сайте uteplitel36 или по адресу: ул. ккккккккккккк или заказать с доставкой по тел: +7(000)000-00-00');

insert into category(id, name, description, keyword)
values (6, 'osb', 'OSB-3 (ОСП)', 'ОСП, ОСБ, OSB купить в Воронеже на сайте uteplitel36 или по адресу: ул. ккккккккккккк или заказать с доставкой по тел: +7(000)000-00-00');

insert into category(id, name, description, keyword)
values (7, 'membrana', 'Мембрана', 'Профилированную мембрану PLASTGUARD купить по лучшей цене в Воронеже на сайте uteplitel36 или по адресу: ул. ккккккккккккк или заказать с доставкой по тел: +7(000)000-00-00');

insert into item(id, code, name, brand, price, image, category)
values (8, 'S001', 'Экструзионный пенополистирол ПЕНОПЛЭКС КОМФОРТ® толщиной 50 мм', 'penoplex', 1850.00, '/static/img/items/penoplex/1-komfort-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (9, 'S002', 'Экструзионный пенополистирол ПЕНОПЛЭКС КОМФОРТ® толщиной 20 мм', 'penoplex', 2400.00, '/static/img/items/penoplex/1-komfort-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (10, 'S003', 'Экструзионный пенополистирол ПЕНОПЛЭКС КОМФОРТ® толщиной 30 мм', 'penoplex', 2236.00, '/static/img/items/penoplex/1-komfort-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (11, 'S004', 'Экструзионный пенополистирол ПЕНОПЛЭКС КОМФОРТ® толщиной 40 мм', 'penoplex', 2025.00, '/static/img/items/penoplex/1-komfort-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (12, 'S005', 'Экструзионный пенополистирол ПЕНОПЛЭКС КОМФОРТ® толщиной 100 мм', 'penoplex', 2300.00, '/static/img/items/penoplex/1-komfort-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (13, 'S006', 'Экструзионный пенополистирол ПЕНОПЛЭКС ФУНДАМЕНТ® толщиной 50 мм', 'penoplex', 2090.00, '/static/img/items/penoplex/1-fundament-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (14, 'S007', 'Экструзионный пенополистирол ПЕНОПЛЭКС СТЕНА® толщиной 50 мм', 'penoplex', 2030.00, '/static/img/items/penoplex/1-stena-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (15, 'S008', 'Экструзионный пенополистирол ПЕНОПЛЭКС СТЕНА® толщиной 30 мм', 'penoplex', 2455.00, '/static/img/items/penoplex/1-stena-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (16, 'S009', 'Утеплитель IZOBEL Л-25 толщиной 50 мм', 'isovol', 530.00, '/static/img/items/izobel/Izobel-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (17, 'S010', 'Утеплитель IZOBEL Л-25 толщиной 100 мм', 'isovol', 675.00, '/static/img/items/izobel/Izobel-228x228.jpg', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (18, 'S011', 'Утеплитель IZOVOL Л-35 толщиной 50 мм', 'isovol', 849.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (19, 'S012', 'Утеплитель IZOVOL Л-35 толщиной 100 мм', 'isovol', 849.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (20, 'S013', 'Утеплитель IZOVOL СТ-50 толщиной 50 мм', 'isovol', 1089.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (21, 'S014', 'Утеплитель IZOVOL СТ-50 толщиной 100 мм', 'isovol', 1089.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (22, 'S015', 'Утеплитель IZOVOL СТ-75 толщиной 50 мм', 'isovol', 2185.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (23, 'S016', 'Утеплитель IZOVOL СТ-75 толщиной 100 мм', 'isovol', 2185.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (24, 'S017', 'Утеплитель IZOVOL СТ-90 толщиной 50 мм', 'isovol', 2352.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (25, 'S018', 'Утеплитель IZOVOL СТ-90 толщиной 100 мм', 'isovol', 2352.00, '/static/img/items/izovol/izovol-228x228.png', 'uteplitel');

insert into item(id, code, name, brand, price, image, category)
values (26, 'S019', 'Клей-пена профессиональная', 'fastfix', 420.00, '/static/img/items/pena/Pnp-FF_310818_glue_PRO-228x228.png', 'pena');

insert into item(id, code, name, brand, price, image, category)
values (27, 'S020', 'Плита OSB-3 (9,0х2500х1250мм) Муром', 'osb', 420.00, '/static/img/items/osb/181_big-228x228.jpg', 'osb');

insert into item(id, code, name, brand, price, image, category)
values (28, 'S021', 'Профилированная мембрана PLASTGUARD 400 (2Х20м)', 'PLASTGUARD', 115.00, '/static/img/items/membrana/membrana-planter-228x228.jpg', 'membrana');

insert into item(id, code, name, brand, price, image, category)
values (29, 'S022', 'Профилированная мембрана PLASTGUARD 500 (2Х20м)', 'PLASTGUARD', 138.00, '/static/img/items/membrana/membrana-planter-228x228.jpg', 'membrana');

insert into item(id, code, name, price, image, category)
values (30, 'S023', 'Держатель для изоляции Рондоль 60 мм', 2.20, '/static/img/items/krepez/rondolchernaya-60-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (31, 'S024', 'Дюбель для теплоизоляции с металлическим гвоздем 10*90 мм (IZM)', 5.50, '/static/img/items/krepez/izm-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (32, 'S025', 'Дюбель для теплоизоляции с металлическим гвоздем 10*120 мм (IZM)', 6.50, '/static/img/items/krepez/izm-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (33, 'S026', 'Дюбель для теплоизоляции с металлическим гвоздем 10*160 мм (IZM)', 7.50, '/static/img/items/krepez/izm-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (34, 'S027', 'Дюбель для теплоизоляции с пластиковым гвоздем 10*80 мм (IZO)', 3.20, '/static/img/items/krepez/izo-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (35, 'S028', 'Дюбель для теплоизоляции с пластиковым гвоздем 10*90 мм (IZO)', 3.40, '/static/img/items/krepez/izo-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (36, 'S029', 'Дюбель для теплоизоляции с пластиковым гвоздем 10*100 мм (IZO)', 3.80, '/static/img/items/krepez/izo-228x228.jpg', 'krepez');

insert into item(id, code, name, price, image, category)
values (37, 'S030', 'Дюбель для теплоизоляции с пластиковым гвоздем 10*120 мм (IZO)', 4.20, '/static/img/items/krepez/izo-228x228.jpg', 'krepez');

insert into item(id, code, name, brand, price, image, category)
values (38, 'S031', 'Еврокрон А (Ветро-влагоизоляция) 30 м2', 'eurokon', 540.00, '/static/img/items/plenki/EvrokronA-228x228.jpg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (39, 'S032', 'Еврокрон А (Ветро-влагоизоляция) 70 м2', 'eurokon', 1020.00, '/static/img/items/plenki/EvrokronA-228x228.jpg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (40, 'S033', 'Еврокрон В (Пароизоляция) 30 м2', 'eurokon', 470.00, '/static/img/items/plenki/EvrokronB-228x228.jpg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (41, 'S034', 'Еврокрон В (Пароизоляция) 70 м2', 'eurokon', 880.00, '/static/img/items/plenki/EvrokronB-228x228.jpg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (42, 'S035', 'Еврокрон D (Гидро-пароизоляция повышенной прочности) 30 м2', 'eurokon', 570.00, '/static/img/items/plenki/EvrokronD-228x228.jpg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (43, 'S036', 'Еврокрон D (Гидро-пароизоляция повышенной прочности) 70 м2', 'eurokon', 1140.00, '/static/img/items/plenki/EvrokronD-228x228.jpg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (44, 'S037', 'Пленка Изолайк А 70 м2 (ветро-влагоизоляция)', 'izolike', 1390.00, '/static/img/items/plenki/Plenkiizolajk-228x228.jpeg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (45, 'S038', 'Пленка Изолайк В 30 м2 (пароизоляция)', 'izolike', 620.00, '/static/img/items/plenki/Plenkiizolajk-228x228.jpeg', 'plenki');

insert into item(id, code, name, brand, price, image, category)
values (46, 'S039', 'Пленка Изолайк D 30 м2 (гидро-пароизоляция повышенной прочности)', 'izolike', 840.00, '/static/img/items/plenki/Plenkiizolajk-228x228.jpeg', 'plenki');

<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page >
    <div class="col-sm-12">
        <div class="iq-card-body">
            <div class="row">
                <div class="col-md-5 offset-md-1">
                    <div class="iq-card">
                        <div class="iq-card-body">
                            <div class="post-item">
                                <div class="user-post" style="display: flex; justify-content: center;">
                                    <img src="${product.image}" alt="${product.name}" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="post-modal-data" class="iq-card">
                        <div class="iq-card-body" data-toggle="modal" data-target="#post-modal">
                            <div class="profile-detail d-flex">
                                <div class="user-data-block">
                                    <h3 class="mb-3">${product.name}</h3>
                                    <i class="fa fa-truck" style="margin: -3px 0px -12px -6px;"></i><span>Доставка по Воронежу</span>
                                    <div class="text-muted mb-2"><a href="/shipping"> <small>Узнайте больше о
                                                доставке</small></a></div>
                                    <p style="font-size: 30px; margin-bottom: 0">${product.price}р.</p>
                                </div>
                            </div>
                            <#if product.inStock??>
                                <#if product.inStock>
                                    <p class="text-success">Есть в наличии</p>
                                <#else>
                                    <p class="text-danger">Нет в наличии</p>
                                </#if>
                            <#else>
                                <p class="text-success">Есть в наличии</p>
                            </#if>

                            <#--                            <label for="quant">Количество</label>-->
                            <#--                            <input type="number" name="quantity" min="1" id="quant" class="form-control mb-5 input-lg"-->
                            <#--                                   placeholder="Choose the quantity">-->
                            <div class="justify-content-center d-flex">

                                <a href="/buyProduct?code=${product.code}"
                                   type="submit"
                                   class="row btn btn-primary mb-3 ml-0">
                                    <div data-icon="Q" class="icon">
                                        Купить
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <#--            <div class="iq-card-header d-flex justify-content-between">-->

        <#--            </div>-->


        <div class="iq-card-body">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="iq-card">
                        <div class="iq-card-body">
                            <div class="iq-header-title">
                                <h4 class="card-title">Характеристики</h4>
                            </div>
                            <table class="table table-striped">
                                <tbody>
                                <#if product.brand??>
                                    <tr>
                                        <th scope="row">Бренд: </th>
                                        <td>${product.brand}</td>
                                    </tr>
                                </#if>
                                <#if product.model??>
                                    <tr>
                                        <th scope="row">Модель: </th>
                                        <td>${product.model}</td>
                                    </tr>
                                </#if>
                                <#if product.measure??>
                                    <tr>
                                        <th scope="row">Размер: </th>
                                        <td>${product.measure}</td>
                                    </tr>
                                </#if>
                                <#if product.count??>
                                    <tr>
                                        <th scope="row">В упаковке: </th>
                                        <td>${product.count} шт</td>
                                    </tr>
                                </#if>
                                <#if product.square??>
                                    <tr>
                                        <th scope="row">Площадь покрытия: </th>
                                        <td>${product.square} м²</td>
                                    </tr>
                                </#if>
                                <#if product.capacity??>
                                    <tr>
                                        <th scope="row">Объём упаковки: </th>
                                        <td>${product.capacity} м³</td>
                                    </tr>
                                </#if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <#if product.description??>
            <div class="iq-card-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="iq-card">
                            <div class="iq-card-body">
                                <div class="iq-header-title">
                                    <h4 class="card-title">Описание</h4>
                                </div>
                                ${product.description}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </#if>
    </div>
</@c.page>

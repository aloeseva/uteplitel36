<#import "parts/common.ftl" as c>

<@c.page>
    <div class="col-md-10 offset-md-1">
        <div class="tab-content">
            <div class="iq-card">
                <div class="iq-card-body">
                    <h4><b>Контакты</b></h4>
                    <p class="col-md-12 mt-2">
                        <br><b>Телефон: </b>+7 (980) 545-93-15
                        <br><b>Адрес: </b>1-я Садовая улица
                        <br>Советский район, Воронеж, Россия
                        <br><br>
                        <script type="text/javascript" charset="utf-8" async
                                src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aaccd6a1ebdd2faa50ff3e5ffc4f9471b3f87ad4265ab4d94ef339462120a5106&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true">
                        </script>
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>

</@c.page>
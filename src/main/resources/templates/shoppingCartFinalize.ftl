<#import "parts/common.ftl" as c>

<@c.page >
    <div class="col-sm-12">
        <div class="tab-content">
            <div class="iq-card">
                <div class="iq-card-body">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <h2>Спасибо за заказ!</h2>
                            <p>
                                <br>Сообщение с информацией о товаре отправлено на вашу почту: <b>${customerEmail}</b><br>
                                В ближайшее время сотрудник нашей фирмы свяжется с вами для уточнения деталей заказа.<br>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 offset-md-1">
                                <span class="dark-color justify-content-center d-flex line-height-2 mt-2">
                                    <a
                                            type="button"
                                            class="btn btn-primary"
                                            href="/">Вернуться к покупкам
                                    </a>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@c.page>

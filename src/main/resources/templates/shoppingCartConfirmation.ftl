<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page >
    <div class="col-sm-12">
        <div class="tab-content">
            <div class="iq-card">
                <div class="iq-card-body">
                    <h2>Подтверждение заказа</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="iq-card">
            <div class="iq-card-body">
                <div class="friend-list-tab mt-2">
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="all-friends" role="tabpanel">
                            <div class="iq-card-body p-0">
                                <div class="row">
                                    <#list myCart.cartLines as productList>
                                        <div class="col-md-3">
                                            <div class="iq-card">
                                                <div class="iq-card-body profile-page p-0">
                                                    <div class="profile-header-image">
                                                        <div class="cover-container justify-content-center d-flex"
                                                                <#--                                                             style="transform: translate(25%, 0);width: 150px;"-->
                                                        >
                                                            <img src="${productList.productInfo.image}"
                                                                 alt="profile-bg" class="rounded img-fluid w-100">
                                                        </div>
                                                        <div class="profile-info p-4">
                                                            <div class="user-detail">
                                                                <div class="flex-wrap justify-content-between align-items-start">
                                                                    <div class="profile-detail d-flex">
                                                                        <div class="user-data-block">
                                                                            <h4>${productList.productInfo.price}p.</h4>
                                                                            <p style="font-size: 11px">${productList.productInfo.name}</p>

                                                                            <p>Количество: ${productList.quantity}</p>
                                                                            <p>Итоговая цена: ${productList.amount}
                                                                                p.</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </#list>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content col-sm-12">
        <div class="iq-card">
            <div class="iq-card-body">
                <div class="row">
                    <div class="col-md-9 pl-4">
                        <div class="tab-content">
                            <h4>Информация заказчика:</h4>
                            <hr>
                            <div class="row">
                                <div class="col-3">
                                    <h6>Имя:</h6>
                                </div>
                                <div class="col-9">
                                    <#if myCart.customerInfo.firstname??>
                                        <p class="mb-0">${myCart.customerInfo.firstname}</p>
                                    </#if>
                                </div>
                                <div class="col-3">
                                    <h6>Фамилия:</h6>
                                </div>
                                <div class="col-9">
                                    <#if myCart.customerInfo.secondname??>
                                        <p class="mb-0">${myCart.customerInfo.secondname}</p>
                                    </#if>
                                </div>
                                <div class="col-3">
                                    <h6>Email:</h6>
                                </div>
                                <div class="col-9">
                                    <p class="mb-0">${myCart.customerInfo.email}</p>
                                </div>
                                <div class="col-3">
                                    <h6>Телефон:</h6>
                                </div>
                                <div class="col-9">
                                    <#if myCart.customerInfo.phone??>
                                        <p class="mb-0">+7 ${myCart.customerInfo.phone}</p>
                                    </#if>
                                </div>
                                <div class="col-3">
                                    <h6>Адрес:</h6>
                                </div>
                                <div class="col-9">
                                    <p class="mb-0">${myCart.customerInfo.address}</p>
                                </div>
                                <div class="col-3">
                                    <h6>Способ получения:</h6>
                                </div>
                                <div class="col-9">
                                    <p class="mb-0">${myCart.customerInfo.shipping}</p>
                                </div>
                            </div>
                            <h4 class="mt-3">Итог по заказу:</h4>
                            <hr>
                            <div class="row">
                                <div class="col-3">
                                    <h6>Количество:</h6>
                                </div>
                                <div class="col-9">
                                    <p class="mb-0">${myCart.quantityTotal}</p>
                                </div>
                                <div class="col-3">
                                    <h6>Итоговая цена:</h6>
                                </div>
                                <div class="col-9">
                                    <p class="mb-0">${myCart.amountTotal}p.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content col-sm-12">
        <div class="iq-card">
            <div class="iq-card-body">
                <form class="col-md-12" action="/shoppingCartConfirmation" method="post">
                    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                    <input type="hidden" name="isUserAuth" <#if user??>value="1"<#else>value="2"</#if>/>
                    <div class="d-inline-block w-100">
                        <div class="row">
                            <div class="col-md-3">
                                <span class="dark-color justify-content-center d-flex line-height-2 mt-2">
                                    <a
                                            type="button"
                                            class="btn btn-primary"
                                            href="/shoppingCart">Изменить корзину
                                    </a>
                                </span>
                            </div>
                            <div class="col-md-4">
                                <span class="dark-color justify-content-center d-flex line-height-2 mt-2">
                                    <a
                                            type="button"
                                            class="btn btn-primary"
                                            href="/shoppingCartCustomer">Изменить информацию покупателя
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="sign-info mt-3 pt-3">
                        <div class="row">
                            <div class="col-md-3 offset-md-9 justify-content-center d-flex">
                                <button type="submit"
                                        style="width: 288px"
                                        class="btn btn-success">Заказать
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</@c.page>

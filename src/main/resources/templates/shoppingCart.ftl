<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">
<#import "/spring.ftl" as spring />

<@c.page>
    <script>
        function increaseValue(index) {
            var value = parseInt(document.getElementById(index).value, 10);
            value = isNaN(value) ? 1 : value;
            value++;
            document.getElementById(index).value = value;
        }

        function decreaseValue(index) {
            var value = parseInt(document.getElementById(index).value, 10);
            value = isNaN(value) ? 1 : value;
            value < 2 ? value = 2 : '';
            value--;
            document.getElementById(index).value = value;
        }
    </script>
    <div class="col-sm-12">
        <div class="tab-content">
            <div class="iq-card">
                <div class="iq-card-body">
                    <h2>Корзина</h2>
                </div>
            </div>
        </div>
    </div>

    <#if cartSize == 0>
        <div class="col-sm-12">
            <div class="tab-content">
                <div class="iq-card">
                    <div class="iq-card-body">
                        <h3>Ваша Корзина пуста!</h3>
                        <span class="dark-color d-inline-block line-height-2">
                            <a href="/">Вернуться к покупкам</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    <#else>
        <div class="tab-content col-md-12">
            <div class="iq-card">
                <div class="iq-card-body">
                    <form class="mt-4 col-md-12" action="/shoppingCart" method="post">
                        <div class="row">
                            <#list cartForm.cartLines as productList>
                                <div class="col-md-3">
                                    <div class="iq-card">
                                        <div class="iq-card-body profile-page p-0">
                                            <div class="profile-header-image">
                                                <div class="cover-container justify-content-center d-flex">
                                                    <a href="/item/${productList.productInfo.code}">
                                                        <img src="${productList.productInfo.image}"
                                                             alt="profile-bg" class="rounded img-fluid w-100">
                                                    </a>
                                                </div>
                                                <div class="profile-info p-4">
                                                    <div class="user-detail">
                                                        <div class="flex-wrap justify-content-between align-items-start">
                                                            <div class="profile-detail d-flex">
                                                                <div class="user-data-block">
                                                                    <h4>${productList.productInfo.price}p.</h4>
                                                                    <p style="font-size: 11px">${productList.productInfo.name}</p>

                                                                    <div class="form-group required">
                                                                        <label>Количество:</label>
                                                                        <div class="input-group  ">
                                                                            <button style="min-width: 2.5rem"
                                                                                    class="btn btn-decrement btn-outline-secondary btn-minus"
                                                                                    onclick="decreaseValue('${productList?index}')"
                                                                                    value="Decrease Value"
                                                                                    type="button"><strong>−</strong>
                                                                            </button>
                                                                            <input type="text" inputmode="decimal"
                                                                                   id="${productList?index}"
                                                                                   name="quantity"
                                                                                   style="text-align: center"
                                                                                   value="${productList.quantity}"
                                                                                   class="form-control " required=""
                                                                            >
                                                                            <button style="min-width: 2.5rem"
                                                                                    class="btn btn-increment btn-outline-secondary btn-plus"
                                                                                    onclick="increaseValue('${productList?index}')"
                                                                                    value="Increase Value"
                                                                                    type="button"><strong>+</strong>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <p>Итоговая цена ${productList.amount}p.
                                                                    </p>
                                                                    <div class="justify-content-center d-flex">
                                                                        <a href="/shoppingCartRemoveProduct?code=${productList.productInfo.code}"
                                                                           type="button"
                                                                           class="btn btn-outline-danger mb-3">Удалить</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </#list>
                        </div>


                        <input type="hidden" name="_csrf" value="${_csrf.token}"/>

                        <div class="justify-content-center d-flex">
                            <button type="submit"
                                    class="btn btn-primary">Обновить количество товаров
                            </button>
                        </div>
                        <div class="sign-info mt-3 pt-3">
                            <div class="row">
                                <div class="col-md-3">
                                <span class="dark-color justify-content-center d-flex line-height-2 mt-2">
                                    <a
                                            type="button"
                                            class="btn btn-primary"
                                            href="/">Вернуться к покупкам
                                    </a>
                                </span>
                                </div>

                                <div class="col-md-3 offset-md-6">
                                <span class="dark-color justify-content-center d-flex line-height-2 mt-2">
                                    <a href="/shoppingCartCustomer"
                                       type="button"
                                       class="btn btn-primary"
                                       style="width: 189px"
                                    >Оформить заказ </a>
                                </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </#if>

</@c.page>
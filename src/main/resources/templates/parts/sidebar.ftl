<#include "security.ftl">

<!-- Sidebar  -->
<div class="iq-sidebar">
    <div id="sidebar-scrollbar">
        <nav class="iq-sidebar-menu">
            <ul id="iq-sidebar-toggle" class="iq-menu">
                <li>
                    <a href="#ui-elements" class="iq-waves-effect collapsed"
                       data-toggle="collapse" aria-expanded="false"><i
                                class="fas fa-align-left"></i><span>Каталог</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                    <ul id="ui-elements" class="iq-submenu collapse"
                        data-parent="#iq-sidebar-toggle">
                        <li>
                            <a href="#ui-kit"
                               class="iq-waves-effect collapsed"
                               data-toggle="collapse"
                               aria-expanded="false">
<#--                                <i class="las la-campground"></i>-->
                                <span>Утеплитель</span>
                                <i class="ri-arrow-right-s-line iq-arrow-right"></i>
                            </a>
                            <ul id="ui-kit"
                                class="iq-submenu collapse"
                                data-parent="#ui-elements">
                                <li>
                                    <a href="/categories/uteplitel">Все</a></li>
                                <li><a
                                            href="/brands/isovol">Isovol</a></li>
                                <li><a
                                            href="/brands/penoplex">Пеноплэкс</a></li>
                                <li><a
                                            href="/brands/izoroc">ISOROC</a></li>
                                <li><a
                                            href="/brands/ursa">URSA</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/categories/krepez"
                               class="iq-waves-effect collapsed">
                            </i><span>Крепеж</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/membrana"
                               class="iq-waves-effect collapsed">
                            <span>Профилированная <br> мембрана (плантер)</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/osb"
                               class="iq-waves-effect collapsed">
                            <span>OSB-3 (ОСП)</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/pena"
                               class="iq-waves-effect collapsed">
                            <span>Пены</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/plenki"
                               class="iq-waves-effect collapsed">
                            <span>Пленки</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/setki"
                               class="iq-waves-effect collapsed">
                            <span>Сетки</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/smesi"
                               class="iq-waves-effect collapsed">
                            <span>Смеси</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/about" class="iq-waves-effect"><i
                                class="las la-industry"></i><span>О нас</span></a>
                </li>
                <li>
                    <a href="/payment" class="iq-waves-effect"><i
                                class="las la-wallet"></i><span>Оплата</span></a>
                </li>
                <li>
                    <a href="/shipping" class="iq-waves-effect"><i
                                class="las la-truck"></i><span>Доставка</span></a>
                </li>
                <li>
                    <a href="/contacts" class="iq-waves-effect"><i
                                class="las la-map-marker-alt"></i><span>Контакты</span></a>
                </li>
            </ul>
        </nav>
        <div class="p-3"></div>
    </div>
</div>

<!-- End Sidebar  -->

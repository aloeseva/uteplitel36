<#include "security.ftl">

<!-- Sidebar  -->
<div class="iq-sidebar">
    <div id="sidebar-scrollbar">
        <nav class="iq-sidebar-menu">
            <ul id="iq-sidebar-toggle" class="iq-menu">
                <li>
                    <a href="#ui-elements" class="iq-waves-effect <#if !categoryName??> collapsed </#if>"
                       data-toggle="collapse" aria-expanded="<#if !categoryName??> false <#else> true </#if>"><i
                                class="fas fa-align-left"></i><span>Каталог</span><i
                                class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                    <ul id="ui-elements" class="iq-submenu collapse <#if categoryName??> show </#if>"
                        data-parent="#iq-sidebar-toggle">
                        <li>
                            <a href="#ui-kit"
                               class="iq-waves-effect <#if !categoryName??> collapsed <#elseif categoryName == "uteplitel"> </#if>"
                               data-toggle="collapse"
                               aria-expanded="<#if !categoryName??> false <#elseif categoryName != "uteplitel"> true </#if>">
                                <#--                                <i class="las la-campground"></i>-->
                                <span>Утеплитель</span>
                                <i class="ri-arrow-right-s-line iq-arrow-right"></i>
                            </a>
                            <ul id="ui-kit"
                                class="iq-submenu collapse <#if !categoryName??> <#elseif categoryName == "uteplitel"> show</#if>"
                                data-parent="#ui-elements">
                                <li class="<#if !category??> <#elseif !subCategory?? && categoryName == "uteplitel"> active</#if>">
                                    <a href="/categories/uteplitel">Все</a></li>
                                <li class="<#if !brand??> <#elseif brand == "ISOVOL"> active</#if>"><a
                                            href="/brands/isovol">Isovol</a></li>
                                <li class="<#if !brand??> <#elseif brand == "PENOPLEX"> active</#if>"><a
                                            href="/brands/penoplex">Penoplex</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/categories/krepez"
                               class="iq-waves-effect>"
                                <#if !categoryName??><#elseif categoryName == "krepez">
                               style="color: var(--iq-primary)" </#if>
                            </i><span>Крепеж</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/membrana"
                               class="iq-waves-effect"
                                <#if !categoryName??><#elseif categoryName == "membrana">
                               style="color: var(--iq-primary)" </#if>
                            <span>Профилированная <br> мембрана (плантер)</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/osb"
                               class="iq-waves-effect"
                                <#if !categoryName??><#elseif categoryName == "osb">
                               style="color: var(--iq-primary)" </#if>
                            <span>OSB-3 (ОСП)</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/pena"
                               class="iq-waves-effect"
                                <#if !categoryName??><#elseif categoryName == "pena">
                               style="color: var(--iq-primary)" </#if>
                            <span>Пены</span>
                            </a>
                        </li>
                        <li>
                            <a href="/categories/plenki"
                               class="iq-waves-effect"
                                <#if !categoryName??><#elseif categoryName == "plenki">
                               style="color: var(--iq-primary)" </#if>
                            <span>Пленки</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/about" class="iq-waves-effect"><i
                                class="las la-industry"></i><span>О нас</span></a>
                </li>
                <li>
                    <a href="/payment" class="iq-waves-effect"><i
                                class="las la-wallet"></i><span>Оплата</span></a>
                </li>
                <li>
                    <a href="/shipping" class="iq-waves-effect"><i
                                class="las la-truck"></i><span>Доставка</span></a>
                </li>
                <#--                <li>-->
                <#--                    <a href="#" class="iq-waves-effect"><i-->
                <#--                                class="las la-handshake"></i><span>Партнеры</span></a>-->
                <#--                </li>-->
                <li>
                    <a href="/contacts" class="iq-waves-effect"><i
                                class="las la-map-marker-alt"></i><span>Контакты</span></a>
                </li>
                <#--                <#if user??>-->
                <#--                    <li>-->
                <#--                        <a href="#" class="iq-waves-effect"><i-->
                <#--                                    class="las la-user"></i><span>Личный кабинет</span></a>-->
                <#--                    </li>-->
                <#--                </#if>-->
            </ul>
        </nav>
        <div class="p-3"></div>
    </div>
</div>
<!-- End Sidebar  -->

<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page >
    <script>
        width = screen.width;
        if (width > 430) {
            cicle = '' +
                '<div class="col-sm-12">' +
                '   <div class="iq-card"> ' +
                '       <div class="iq-card-body"> ' +
                '           <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">' +
                '               <div class="carousel-inner"> ' +
                '                   <div class="carousel-item  active" align="center">' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/full_jjIbt5ZY-130x100.jpg" alt="PENOPLEX" class="img-responsive">' +
                '                           </a> ' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="carousel-item" align="center">' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/2017-02-06_18-12-38_1.c66f2b361b18fdec0ae32e1185520f09-130x100.jpg" alt="IZOVOL" class="img-responsive"> ' +
                '                           </a>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="carousel-item" align="center">' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/logoevrokron-130x100.png" alt="Еврокрон" class="img-responsive"> ' +
                '                           </a>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="carousel-item" align="center">' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/logoIzoLajk-130x100.jpg" alt="ИзоЛайк" class="img-responsive">' +
                '                           </a> ' +
                '                       </div>' +
                '                       <div class="col-3 float-left"> ' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/Bostik-130x100.jpg" alt="Bostik" class="img-responsive">' +
                '                           </a>' +
                '                       </div> ' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/c6466d162b7e626667c8de72cc540a0d-130x100.jpg" alt="UltraLam" class="img-responsive">' +
                '                           </a>' +
                '                       </div>' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/40dbfb12948fee04a46e03100c5bea23-130x100.jpg" alt="Технониколь" class="img-responsive"> ' +
                '                           </a>' +
                '                       </div>' +
                '                   </div>' +
                '                   <div class="carousel-item" align="center">' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/9599fcca5cd7076109e774bb8105b503-130x100.jpg" alt="Tech-KREP" class="img-responsive">' +
                '                           </a> ' +
                '                       </div>' +
                '                       <div class="col-3 float-left"> ' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/logoevrokron-130x100.png" alt="Еврокрон" class="img-responsive">' +
                '                           </a>' +
                '                       </div> ' +
                '                       <div class="col-3 float-left">' +
                '                           <a href="#">' +
                '                               <img src="https://stroydiskont-vrn.ru/image/cache/catalog/logoizover-130x100.png" alt="IZOVER" class="img-responsive">' +
                '                           </a>' +
                '                       </div>' +
                '                   </div>' +
                '               </div>' +
                '               <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> ' +
                '                   <span class="carousel-control-prev-icon" aria-hidden="true"> </span> ' +
                '                   <span class="sr-only">Previous</span> ' +
                '               </a> ' +
                '               <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> ' +
                '                   <span class="carousel-control-next-icon" aria-hidden="true"></span> ' +
                '                   <span class="sr-only">Next</span> ' +
                '               </a> ' +
                '           </div>' +
                '       </div> ' +
                '   </div> ' +
                '</div>'
            // '</div> '
        } else {
            cicle = ''
        }
    </script>
    <div class="col-sm-12">
        <div class="iq-card">
            <div class="iq-card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <a HREF="/brands/penoplex">
                                <img src="/static/img/penoplex_banner.jpg" class="d-block w-100" alt="#">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="/brands/isovol">
                                <img src="/static/img/izovolbaner-1140x380.png" class="d-block w-100" alt="#">
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="/brands/izoroc">
                                <img src="/static/img/SOGREVAETTAM,GDEDRUGIEBESSILNI-1140x380.png" class="d-block w-100"
                                     alt="#">
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="tab-content">
            <div class="iq-card">
                <div class="iq-card-body">
                    <h2>Каталог</h2>
                    <div class="friend-list-tab mt-2">
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="all-friends" role="tabpanel">
                                <div class="iq-card-body p-0">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/uteplitel">
                                                            <img src="/static/img/catalog/1-300x300.jpg"
                                                                 width="150px" height="150px" alt="Утеплители"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/uteplitel">
                                                                <h5>Утеплители</h5>
                                                                <p class="mb-0">29</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/plenki">
                                                            <img src="/static/img/catalog/plenkakategoriya-300x300.png"
                                                                 width="150px" height="150px" alt="Пленки"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/plenki">
                                                                <h5>Пленки</h5>
                                                                <p class="mb-0">9</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/krepez">
                                                            <img src="/static/img/catalog/krepezh-300x300.jpg"
                                                                 width="150px" height="150px" alt="Крепеж"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/krepez">
                                                                <h5>Крепеж</h5>
                                                                <p class="mb-0">8</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/pena">
                                                            <img src="/static/img/catalog/peni-300x300.png"
                                                                 width="150px" height="150px" alt="Пены"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/pena">
                                                                <h5>Пены</h5>
                                                                <p class="mb-0">1</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/osb">
                                                            <img src="/static/img/catalog/OSB-3KRONO-UKRAINA,18MM33-500x500-300x300.jpg"
                                                                 width="150px" height="150px" alt="OSB-3 (ОСП)"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/osb">
                                                                <h5>OSB-3 (ОСП)</h5>
                                                                <p class="mb-0">1</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/membrana">
                                                            <img src="static/img/items/membrana/membrana-planter-228x228.jpg"
                                                                 width="150px" height="150px" alt="Мембрана"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/membrana">
                                                                <h5>Мембрана</h5>
                                                                <p class="mb-0">2</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/setki">
                                                            <img src="static/img/items/setki/1-760x450-300x300.jpg"
                                                                 width="150px" height="150px" alt="Сетки"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/setki">
                                                                <h5>Сетки</h5>
                                                                <p class="mb-0">2</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 mb-3">
                                            <div class="iq-friendlist-block">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <div class="d-flex align-items-center">
                                                        <a href="/categories/smesi">
                                                            <img src="static/img/items/smesi/Bezimyannij-300x300.png"
                                                                 width="150px" height="150px" alt="Смеси"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="friend-info ml-3">
                                                            <a href="/categories/smesi">
                                                                <h5>Смеси</h5>
                                                                <p class="mb-0">2</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 offset-md-4">
        <div class="iq-card">
            <div class="iq-card-body p-0">
                <div class="user-tabing">
                    <ul class="nav nav-pills d-flex align-items-center justify-content-center profile-feed-items p-0 m-0">
                        <li class="col-sm-6 p-0">
                            <a class="nav-link active" data-toggle="pill" href="#recommend">Рекомендуемые</a>
                        </li>
                        <li class="col-sm-6 p-0">
                            <a class="nav-link" data-toggle="pill" href="#bestseller">Хиты продаж</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="tab-content">
            <div class="tab-pane fade active show" id="recommend" role="tabpanel">
                <div class="iq-card">
                    <div class="iq-card-body">
                        <div class="friend-list-tab mt-2">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="all-friends" role="tabpanel">
                                    <div class="iq-card-body p-0">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S004">
                                                                    <img src="/static/img/items/penoplex/1-komfort-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S004"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2290p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС КОМФОРТ®
                                                                                    толщиной 40 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S002">
                                                                    <img src="/static/img/items/penoplex/1-komfort-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S002"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2720p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС КОМФОРТ®
                                                                                    толщиной 20 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S003">
                                                                    <img src="/static/img/items/penoplex/1-komfort-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S003"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2535p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС КОМФОРТ®
                                                                                    толщиной 30 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S001">
                                                                    <img src="/static/img/items/penoplex/1-komfort-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S001"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2150p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС КОМФОРТ®
                                                                                    толщиной 50 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S005">
                                                                    <img src="/static/img/items/penoplex/1-komfort-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S005"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2550p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС КОМФОРТ®
                                                                                    толщиной 100 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S006">
                                                                    <img src="/static/img/items/penoplex/1-fundament-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S006"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2400p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС ФУНДАМЕНТ®
                                                                                    толщиной 50 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S007">
                                                                    <img src="/static/img/items/penoplex/1-stena-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S007"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2350p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС СТЕНА®
                                                                                    толщиной 50 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S008">
                                                                    <img src="/static/img/items/penoplex/1-stena-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S008"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">2650p.</h4>
                                                                                <p style="font-size: 11px">Экструзионный
                                                                                    пенополистирол ПЕНОПЛЭКС СТЕНА®
                                                                                    толщиной 30 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="bestseller" role="tabpanel">
                <div class="iq-card">
                    <div class="iq-card-body">
                        <div class="friend-list-tab mt-2">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="all-friends" role="tabpanel">
                                    <div class="iq-card-body p-0">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S024">
                                                                    <img src="/static/img/items/krepez/izm-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S024"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">6,5p.</h4>
                                                                                <p style="font-size: 11px">Дюбель для
                                                                                    теплоизоляции с
                                                                                    металлическим гвоздем 10*90 мм (IZM)
                                                                                    - шт</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S026">
                                                                    <img src="/static/img/items/krepez/izm-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S026"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">10p.</h4>
                                                                                <p style="font-size: 11px">Дюбель для
                                                                                    теплоизоляции с металлическим
                                                                                    гвоздем 10*160 мм (IZM)</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S029">
                                                                    <img src="/static/img/items/krepez/izo-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S029"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">4,4p.</h4>
                                                                                <p style="font-size: 11px">Дюбель для
                                                                                    теплоизоляции с пластиковым гвоздем
                                                                                    10*100 мм (IZO)</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="iq-card" style="height: 450px">
                                                    <div class="iq-card-body profile-page p-0">
                                                        <div class="profile-header-image">
                                                            <div class="cover-container">
                                                                <a href="/item/S010">
                                                                    <img src="/static/img/items/izobel/Izobel-228x228.jpg"
                                                                         alt="profile-bg"
                                                                         class="rounded img-fluid w-100">
                                                                </a>
                                                            </div>
                                                            <div class="profile-info p-4">
                                                                <div class="user-detail">
                                                                    <div class="d-flex flex-wrap justify-content-center align-items-start">
                                                                        <a href="/buyProduct?code=S010"
                                                                           type="submit"
                                                                           class="row btn btn-primary mb-3"
                                                                                <#--                                                                           style="transform: translate(50%, 0)"-->
                                                                        >
                                                                            <div data-icon="Q" class="icon">
                                                                                Купить
                                                                            </div>
                                                                        </a>
                                                                        <div class="profile-detail d-flex">
                                                                            <div class="user-data-block">
                                                                                <h4 class="">680p.</h4>
                                                                                <p style="font-size: 11px">Утеплитель
                                                                                    IZOBEL Л-25 толщиной 100 мм</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@c.page>